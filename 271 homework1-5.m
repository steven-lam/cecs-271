%1
x = 0:.1:1;
f = x.^2;
plot(x,f);
%6
x = 0:.1:2;
f = x.^3+1;
plot(x,f);
%2
x = 1:.1:2;
f = x.^2-5*x+6;
plot(x,f);
%7
x = 1:.1:2;
f = (x.^2.+3)./(2.*x.^2.-1);
plot(x,f);
%3
x = 1:.1:4;
f = x.^2-5*x+6;
plot(x,f);
%8
x = .5:.1:1.5;
f = 1./x;
plot(x,f);
%4
x = 0:.1:1;
f = x.^3;
plot(x,f);
%5
x = 0:.1:1;
f = x-x.^2;
plot(x,f);
%10
x = 0:.1:5;
f = sqrt(x);
plot(x,f);
%9
x = 0:.1:2;
f = log(2.*x.^2+5);
plot(x,f);
