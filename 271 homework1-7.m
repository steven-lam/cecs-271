k=input('Enter a root:')
n=input('Enter a natural number')
xN = 1.0 / k;
x0 = xN;
xN = (1.0/k) * ((k-1) * x0 + n / (x0 ^ (k-1)));
while(floor(1000 * xN) ~= floor(1000 * x0))
    x0 = xN
    xN = (1.0/k) * ((k-1) * x0 + n / (x0 ^ (k-1)));
end
'The cube root of ',num2str(n),' = ',num2str(xN)