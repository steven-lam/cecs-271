import java.util.Scanner;
import java.text.*;
public class NewtonsMethod {
    public static void main(String[] args) {
        int n,k;
        double xN, x0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value: ");
        k=sc.nextInt();
        System.out.println("Enter a Natural Numer: ");
        n=sc.nextInt();
        DecimalFormat df = new DecimalFormat("0.00#");
        xN =(1.0/k);
        x0=xN;
        xN=(1.0/k)*(((k-1.0)*x0)+n/(Math.pow(x0,k-1.0)));
        while((int)(1000.0*xN)/1000.0 != (int)(1000.0*x0)/1000.0) 
        {
            System.out.println(df.format(xN));
            x0=xN;
            xN=(1.0/k)*(((k-1.0)*x0)+n/(Math.pow(x0,k-1.0)));
        }
        System.out.println("The " + k + " root of: " + n + " = "+df.format(xN));
    }
}